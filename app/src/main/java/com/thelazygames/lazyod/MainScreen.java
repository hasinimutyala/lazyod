package com.thelazygames.lazyod;

import android.view.MotionEvent;
import android.view.View;

public class MainScreen extends GameView {
    int m=20;
    MainScreen(Game game){
        super(game);
    }
@Override
    public void update(float dt){
        Touc t= game.getTouch();
        if(t.up){
            t.up = false;
            game.setGameView(new Help(game));
            return;
        }
        if(m==0){
            m=20;
        }
        else{
            m--;
        }

    }
    @Override
    public void draw(float dt){
        Graphics g = game.getGraphics();
        g.drawBmp(Assets.title, 0, 0);
        if(m <= 10)
            g.drawBmp(Assets.tts, 311, 260);
    }
    @Override
    public void pause(){

    }
    @Override
    public void resume(){

    }
    @Override
    public void dispose(){

    }

}




