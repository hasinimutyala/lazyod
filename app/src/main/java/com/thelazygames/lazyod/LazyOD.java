package com.thelazygames.lazyod;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

public class LazyOD extends Activity implements Game {
    WakeLock wakeLock;
    GameView gameView;
    MyView myView;
    Graphics graphics;
    Audio audio;
    Touc touc;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int ngw = 800;
        int ngh = 480;


        Bitmap ng = Bitmap.createBitmap(ngw, ngh, Bitmap.Config.RGB_565);
        float cgw = getWindowManager().getDefaultDisplay().getWidth();
        float cgh = getWindowManager().getDefaultDisplay().getHeight();



        String c="h";

        float game_ratio = (float)ngw / ngh;
        float cur_ratio = (float)cgw / cgh;

        if(cur_ratio > game_ratio) {
            cgw = (cgh * game_ratio);
        }
        else {
            cgh = (float)cgw / game_ratio;
        }



        float mT = ((getWindowManager().getDefaultDisplay().getHeight())-cgh) / 2;
        float mB = cgh+mT;
        float mL = ((getWindowManager().getDefaultDisplay().getWidth()-cgw)) / 2;
        float mR = cgw+mL;

        float sx = (float) ngw / cgw;
        float sy = (float) ngh / cgh;


        myView = new MyView(this,ng,mT,mR,mB,mL);
        graphics = new Graphics(getAssets(), ng, c);
        touc = new Touc(myView, sx, sy, mT, mL);
        audio = new Audio(this);
        gameView = new LoadAll(this);
        setContentView(myView);


        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "Game");
    }
    @Override
    public void onResume(){
        super.onResume();
        wakeLock.acquire();
        gameView.resume();
        myView.resume();
    }
    @Override
    public void onPause(){
        super.onPause();
        wakeLock.release();
        myView.pause();
        gameView.pause();


        if(isFinishing())
            gameView.dispose();
    }

    public Graphics getGraphics(){
        return graphics;
    }
    public Audio getAudio(){
        return audio;
    }
    public void setScore(int s){
        if(getScore()<s) {
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("score", s);
            editor.apply();
        }
    }
    public int getScore(){
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        return prefs.getInt("score", 0);
    }

    public void setGameView(GameView gameView){
        if(gameView == null)
            throw new IllegalArgumentException("GameView is empty");
        this.gameView.pause();
        this.gameView.dispose();
        gameView.resume();
        gameView.update(0);
        this.gameView = gameView;
    }

    public GameView getCurrentGameView(){
        return gameView;
    }
    public Touc getTouch(){
        return touc;
    }

}
