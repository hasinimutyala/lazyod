package com.thelazygames.lazyod;

import android.media.SoundPool;

public class Sound {
    int s_id;
    SoundPool soundPool;
    public Sound(SoundPool soundPool, int s_id){
        this.s_id = s_id;
        this.soundPool = soundPool;
    }
    public void play(float v){
        soundPool.play(s_id, v, v, 0, 0 , 1);
    }
    public void dispose(){
        soundPool.unload(s_id);
    }
}
