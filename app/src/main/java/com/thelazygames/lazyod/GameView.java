package com.thelazygames.lazyod;

public abstract class GameView {
    protected final Game game;
    public GameView(Game game){
        this.game = game;
    }
    public abstract void draw(float dt);
    public abstract void update(float dt);
    public abstract void pause();
    public abstract void resume();
    public abstract void dispose();
}
