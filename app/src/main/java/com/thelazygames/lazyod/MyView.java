package com.thelazygames.lazyod;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MyView extends SurfaceView implements Runnable {
    LazyOD game;
    Bitmap ng;
    Thread thread = null;
    SurfaceHolder holder;
    volatile boolean running = false;
    float mT,mR,mB,mL;
    public MyView(LazyOD game, Bitmap ng,float mT,float mR,float mB,float mL){
        super(game);
        this.game = game;
        this.ng = ng;
        this.holder = getHolder();
        this.mT = mT;
        this.mR = mR;
        this.mB = mB;
        this.mL = mL;
    }
    public void resume(){
        running = true;
        thread = new Thread(this);
        thread.start();
    }
    public void run(){
        Rect drect = new Rect();
        long st = System.nanoTime();
        while(running){
            if(!holder.getSurface().isValid())
                continue;
            float dt = (System.nanoTime()-st)/ 1000000000.0f;
            st = System.nanoTime();

            game.getCurrentGameView().update(dt);
            game.getCurrentGameView().draw(dt);



            Canvas canvas = holder.lockCanvas();
            canvas.getClipBounds(drect);

            drect.top= (int)this.mT;
            drect.bottom= (int)this.mB;
            drect.left=(int)this.mL;
            drect.right=(int)this.mR;

            canvas.drawBitmap(ng, null, drect, null);
            holder.unlockCanvasAndPost(canvas);
        }
    }
    public void pause(){
        running = false;
        while(true){
            try{
                thread.join();
                return;
            } catch(InterruptedException e){
                //
            }
        }
    }


}
