package com.thelazygames.lazyod;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.io.IOException;
import java.io.InputStream;

public class Graphics {
    AssetManager assets;
    Bitmap ng;
    Canvas canvas;
    Paint paint;
    Rect sRect = new Rect();
    Rect dRect = new Rect();
    String c;


    public Graphics(AssetManager assets, Bitmap ng, String c){
        this.assets = assets;
        this.ng = ng;
        this.canvas = new Canvas(ng);
        this.paint = new Paint();
        this.c = c;
    }

    public Bmp newBmp(String fn,Bmp.BmpFormat format){
        Config config = null;
        if(format == Bmp.BmpFormat.RGB_565)
            config = Config.RGB_565;
        else if(format == Bmp.BmpFormat.ARGB_4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;

        Options options = new Options();
        options.inPreferredConfig = config;

        InputStream in = null;
        Bitmap bitmap = null;
        try{
            in = assets.open(fn);
            bitmap = BitmapFactory.decodeStream(in);
        } catch(IOException e){
            throw new RuntimeException("Could not load Bitmap");
        }
        finally {
            if(in != null){
                try{
                    in.close();
                }
                catch(IOException e){

                }
            }
        }
        if(bitmap.getConfig() == Config.RGB_565)
            format = Bmp.BmpFormat.RGB_565;
        else if(bitmap.getConfig() == Config.ARGB_4444)
            format = Bmp.BmpFormat.ARGB_4444;
        else
            format = Bmp.BmpFormat.ARGB_8888;

        return new Bmp(bitmap, format);

    }

    public void drawBmp(Bmp bmp, int x, int y){
        canvas.drawBitmap(((Bmp) bmp).bitmap, x, y, null);

//        paint.setColor(Color.RED);
//        paint.setTextSize(50);
//        canvas.drawText(this.c,200,200,paint);
    }

    public void drawBmp(Bmp bmp, int x, int y, float r) {
        canvas.save();
        canvas.rotate(r, x+(bmp.getWidth()/2),y+(bmp.getHeight()/2));
        canvas.drawBitmap(((Bmp) bmp).bitmap,x,y,null);
        canvas.restore();
    }

    public void drawBmp(Bmp bmp, int x, int y, int srcX, int srcY, int srcW, int srcH){
        sRect.left = srcX;
        sRect.top = srcY;
        sRect.right = srcX + srcW - 1;
        sRect.bottom = srcY + srcH - 1;

        dRect.left = x;
        dRect.top = y;
        dRect.right = x + srcW - 1;
        dRect.bottom = y + srcH - 1;
        canvas.drawBitmap(((Bmp) bmp).bitmap, sRect, dRect, null);

    }

    public void drawBmp(String s){
        paint.setColor(Color.RED);
        paint.setTextSize(50);
        canvas.drawText(s, 200, 200, paint);
    }

    public void drawBmp(String s,int x,int y,int size){
        paint.setColor(Color.rgb(235,227,23));
        paint.setTextSize(size);
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(s, x, y, paint);
   }



}
