package com.thelazygames.lazyod;

public class Crow {
   int x,y,x1,x2,ax;
   boolean d;
   Bmp sprite;
    Touc touch;
  public Crow(int x,int y,int x2,Game game){
      this.touch = game.getTouch();
      this.x = x;
      this.y = y;
      this.x1 = x;
      this.x2 = x2;
      this.d = false;
      this.sprite = Assets.crow_fly;
  }
  public void draw(Game game){

      Graphics g = game.getGraphics();
      if((this.sprite == Assets.crow_fly)||(this.sprite == Assets.crow_fly_flip))
      g.drawBmp(this.sprite,this.x,this.y, this.ax, 0, 129, 79);
      else
          g.drawBmp(this.sprite,this.x,this.y);
  }
    double tp = 1f/60;
    double time = 5;
    double ftime = 0.15;
  public void update(){
      time += tp;
      if((this.sprite == Assets.crow_fly) || (this.sprite == Assets.crow_fly_flip)) {
          if ((this.x <= this.x2) && (!this.d)) {
              this.x += 5;
              if(this.x==90) {
                  if (GameWorld.aud)
                      Assets.crow_caw_e.play(1);
              }
          } else {
              this.d = true;
              if (this.x >= this.x1) {
                  this.sprite = Assets.crow_fly_flip;
                  this.x -= 5;
                  if(this.x == 400) {
                      if (GameWorld.aud)
                          Assets.crow_caw.play(1);
                  }
              }
              if (this.x == this.x1) {
                  this.d = false;
                  this.sprite = Assets.crow_fly;
              }
          }

          while (time > ftime) {
              time -= ftime;
              if (!this.d) {
                  if (this.ax == 0)
                      this.ax = 131;
                  else
                      this.ax = 0;
              } else {
                  if (this.ax == 131)
                      this.ax = 0;
                  else
                      this.ax = 131;
              }
          }
      }




      if(touch.type==0){
          if (touch.x < this.x + 129 && touch.x > this.x && touch.y < this.y + 79 && touch.y > this.y) {
              if(!this.d)
                  this.sprite = Assets.dead_crow;
              else
                  this.sprite = Assets.dead_crow_flip;
              if(GameWorld.aud) {
                  Assets.touch.play(1);
                  Assets.char_c.play(1);
              }
          }
      }

      if((this.sprite == Assets.dead_crow) || (this.sprite == Assets.dead_crow_flip))
      {
          this.y+=10;
      }

      if(this.y > 480)
      {
          this.sprite = Assets.crow_fly;
          this.x = this.x1;
          this.y = 80;
          this.d = false;
      }
  }
}
