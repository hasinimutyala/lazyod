package com.thelazygames.lazyod;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import java.io.IOException;

public class Audio {
    AssetManager assets;
    SoundPool soundPool;
    public Audio(Activity activity){
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }
    public Sound newSound(String filename){
    try{
        AssetFileDescriptor assetDescriptor = assets.openFd(filename);
        int s_id = soundPool.load(assetDescriptor, 0);
        return new Sound(soundPool, s_id);
    }
    catch(IOException e){
        throw new RuntimeException("file not found");
    }
    }
}

