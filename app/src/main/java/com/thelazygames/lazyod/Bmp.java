package com.thelazygames.lazyod;

import android.graphics.Bitmap;

public class Bmp {
    public static enum BmpFormat{
        ARGB_8888, ARGB_4444, RGB_565
    }
    Bitmap bitmap;
    BmpFormat format;

    public Bmp(Bitmap bitmap, BmpFormat format){
        this.bitmap = bitmap;
        this.format = format;
    }
    public int getWidth(){
        return bitmap.getWidth();
    }
    public int getHeight(){
        return bitmap.getHeight();
    }
    public BmpFormat getForamt(){
        return format;
    }
    public void dispose(){
        bitmap.recycle();
    }

}
