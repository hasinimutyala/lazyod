package com.thelazygames.lazyod;

public class Fruit {
    int x,x1,y,w,h,rot,ax,by;
    Bmp sprite;
    boolean s;
    boolean cstatus;
    int cx,cy,cx1,cx2,cax;
    boolean cd;
    Bmp csprite;
    double cvex;
    double cvey;
    boolean check;
    Touc touch;
    static int fl;
    static int score;
    static boolean fs;

    public Fruit(int x, int y, Bmp sprite, Game game, int cx, int cy, int cx2){
        this.x1 = x;
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.touch = game.getTouch();
        this.w = sprite.getWidth();
        this.h = sprite.getHeight();
        this.cx = cx;
        this.cy = cy;
        this.cx1 = cx;
        this.cx2 = cx2;
        Fruit.fl=30;
        Fruit.score = 0;
        Fruit.fs = false;
        this.rot=0;
        this.ax=0;
        this.by=0;
        this.s= false;
        this.cstatus=false;
        this.cd = false;
        this.csprite = Assets.crow_fly;
        this.cvex = 0;
        this.cvey = 0;
        this.check=false;
    }
    public void draw(Game game){
        Graphics g = game.getGraphics();
        g.drawBmp(this.sprite, this.x, this.y, this.rot);
        if(this.csprite == Assets.crow_fly || this.csprite == Assets.crow_fly_flip)
            g.drawBmp(this.csprite,this.cx,this.cy,this.cax, 0, 129, 79);
        else
            g.drawBmp(this.csprite, this.cx, this.cy);
    }
    double ctp = 1f/60;
    double ctime = 5;
    double cftime = 0.15;
    public void update(Game game) {
        ctime += ctp;
        if (touch.type == 0) {
            if (touch.x < this.x + 59 && touch.x > this.x && touch.y < this.y + 59 && touch.y > this.y) {
                this.ax = touch.x - this.x;
                this.by = touch.y - this.y;
                this.s = true;
                if(GameWorld.aud)
                    Assets.touch.play(1);
            }
        }
        if (touch.type == 2) {
            this.s = false;
        }
        if ((touch.type == 1) && (this.s)) {
            this.x = touch.x - ax;
            this.y = touch.y - by;
        }


        int vx = (this.x + (this.w / 2)) - (-240 + (560 / 2));
        int vy = (this.y + (this.h / 2)) - (400 + (996 / 2));
        int comb_h_w = (this.w / 2) + (560 / 2);
        int comb_h_h = (this.h / 2) + (996 / 2);

        if (Math.abs(vx) < comb_h_w) {
            if (Math.abs(vy) < comb_h_h) {
                int overlapX = comb_h_w - Math.abs(vx);
                int overlapY = comb_h_h - Math.abs(vy);
                if (overlapX >= overlapY) {
                    if (vy > 0) {
                        this.y = this.y + overlapY;
                    } else {
                        this.y = this.y - overlapY;
                    }
                } else {
                    if (vx > 0) {
                        this.x = this.x + overlapX;
                    } else {
                        this.x = this.x - overlapX;
                    }
                }
            }
        }


        int vx2 = (this.x + (this.w / 2)) - (420 + (986 / 2));
        int vy2 = (this.y + (this.h / 2)) - (400 + (996 / 2));
        int comb_h_w2 = (this.w / 2) + (986 / 2);
        int comb_h_h2 = (this.h / 2) + (996 / 2);


        if (Math.abs(vx2) < comb_h_w2) {
            if (Math.abs(vy2) < comb_h_h2) {
                int overlapX2 = comb_h_w2 - Math.abs(vx2);
                int overlapY2 = comb_h_h2 - Math.abs(vy2);
                if (overlapX2 >= overlapY2) {
                    if (vy2 > 0) {
                        this.y = this.y + overlapY2;
                    } else {
                        this.y = this.y - overlapY2;
                    }
                } else {
                    if (vx2 > 0) {
                        this.x = this.x + overlapX2;
                    } else {
                        this.x = this.x - overlapX2;
                    }
                }
            }
        }


        if (!this.s) {
            if (this.y > 339 && this.y < 400 && this.x > -2000 && this.x < 320) {
                this.x += 2;
                if(this.x >0 && this.x<220)
                    Rock.ax = 0;
                else if(this.x >= 220 && this.x<320)
                    Rock.ax = 90;
                else
                    Rock.ax = 180;
//                else if(this.x >= 350)
//                    Rock.ax = 180;

                //rotation code
                if (this.rot >= 360)
                    this.rot = 0;
                else {
                    this.rot += 3;
                }

            } else if (this.y > 339 && this.y < 400 && this.x > 361 && this.x < 1936) {
                this.x -= 2;
                //rotation code
                if (this.rot <= 0)
                    this.rot = 360;
                else
                    this.rot -= 3;
            } else {
                this.y += 5;
            }
        }
        if (this.y > 450) {
            this.x = this.x1;
            this.y = 10;
            if(this.sprite != Assets.black_fruit)
                Fruit.score -= 10;
        }
        if((this.x > this.x1)&&(this.x<(this.x1+40)))
        {
            Fruit.fs = false;
        }
        if(this.x < -100){
            if (this.sprite != Assets.black_fruit) {
                int t = (int) Math.floor((Math.random() * 4) + 1);
                if (t == 1)
                    this.sprite = Assets.red_fruit;
                if (t == 2)
                    this.sprite = Assets.green_fruit;
                if (t == 3)
                    this.sprite = Assets.blue_fruit;
                if (t == 4)
                    this.sprite = Assets.orange_fruit;

            }
        }

        //Crow


            if ((touch.type == 0 ) && (touch.x < this.cx + 129 && touch.x > this.cx && touch.y < this.cy + 79 && touch.y > this.cy)) {

                if(this.csprite == Assets.crow_fly)
                    this.csprite = Assets.dead_crow;
                else
                    this.csprite = Assets.dead_crow_flip;
                if(GameWorld.aud) {
                    Assets.touch.play(1);
                    Assets.char_c.play(1);
                }
            }
        if(this.csprite == Assets.dead_crow || this.csprite == Assets.dead_crow_flip) {
            this.cy += 10;

        }


        if(this.cy > 480)
        {
            this.csprite = Assets.crow_fly;
            this.cx = this.cx1;
            this.cy = 10;
            this.cd = false;
            if(this.cstatus)
                Fruit.score -=10;
            else
                Fruit.score +=50;
        }



if((this.csprite == Assets.crow_fly || this.csprite == Assets.crow_fly_flip) &&(this.sprite != Assets.black_fruit)) {
    if ((this.cx <= this.cx2) && (!this.cd)) {
//            this.cx+=5;
        if (this.y <= 400 && this.x > 100 && !this.s) {
            this.cstatus = false;
            if (this.x < this.cx + 129 && this.x + this.w > this.cx && this.y < this.cy + 79 && this.h + this.y > this.cy) {
                    this.csprite = Assets.crow_fly;
                this.cvex = ((this.cx2 + 100) - this.cx) * 1.2;
                this.cvey = (90 - this.cy) * 1.2;
                this.cx += this.cvex * 0.03;
                this.cy += this.cvey * 0.03;
                this.x = this.cx + 10;
                this.y = this.cy + 40;
                if (this.cx >= cx2) {
                    this.x = this.x1;
                    Fruit.score -= 10;
                }
            } else {
                if (this.x > this.cx) {
                        this.csprite = Assets.crow_fly;
                } else {
                        this.csprite = Assets.crow_fly_flip;
                }
                this.cvex = (this.x - this.cx) * 1.3;
                this.cvey = (this.y - this.cy) * 1.3;
                this.cx += this.cvex * 0.04;
                this.cy += this.cvey * 0.04;
            }
        } else {
            if ((this.cx + 129) > 0) {

                    this.csprite = Assets.crow_fly;
                this.cvex = ((this.cx2 + 1000) - this.cx) * 1.2;
                this.cvey = (90 - this.cy) * 1.2;
                this.cx += this.cvex * 0.03;
                this.cy += this.cvey * 0.03;
            }

        }
        this.check = false;

    } else {
        this.cd = true;
        if (this.cx >= this.cx1) {

                this.csprite = Assets.crow_fly_flip;


            if (this.y <= 400 && this.x > 100 && !this.s && !this.check) {
                if (this.x < this.cx + 129 && this.x + this.w > this.cx && this.y < this.cy + 79 && this.h + this.y > this.cy) {

                        this.csprite = Assets.crow_fly;
                    this.cvex = ((this.cx2 + 100) - this.cx) * 1.2;
                    this.cvey = (90 - this.cy) * 1.2;
                    this.cx += this.cvex * 0.03;
                    this.cy += this.cvey * 0.03;
                    this.x = this.cx + 10;
                    this.y = this.cy + 40;
                    if (this.cx >= cx2) {
                        this.x = this.x1;
                        this.cx = this.cx1;
                        Fruit.score -= 10;
                    }
                } else {
                    if (this.x > this.cx) {

                            this.csprite = Assets.crow_fly;
                    } else {

                            this.csprite = Assets.crow_fly_flip;
                    }
                    this.cvex = (this.x - this.cx) * 1.3;
                    this.cvey = (this.y - this.cy) * 1.3;
                    this.cx += this.cvex * 0.04;
                    this.cy += this.cvey * 0.04;

                }
                this.check = true;
            } else {
                this.cstatus=true;
                this.cx -= 25;
            }

        }
        if (this.cx <= this.cx1) {
            this.cd = false;
            this.csprite = Assets.crow_fly;
        }
    }

    while (ctime > cftime) {
        ctime -= cftime;
        if (this.csprite == Assets.crow_fly) {
            if (this.cax == 0)
                this.cax = 131;
            else
                this.cax = 0;
        } else {
            if (this.cax == 131)
                this.cax = 0;
            else
                this.cax = 131;
        }
    }


}





        //Char

        int cvx = (this.x + (this.w / 2)) - (Char.x + (65 / 2));
        int cvy = (this.y + (this.h / 2)) - ((Char.y+66) + (134 / 2));
        int ccomb_h_w = (this.w / 2) + (65 / 2);
        int ccomb_h_h = (this.h / 2) + (134 / 2);


        if (Math.abs(cvx) < ccomb_h_w) {
            if (Math.abs(cvy) < ccomb_h_h) {
                int coverlapX = ccomb_h_w - Math.abs(cvx);
                int coverlapY = ccomb_h_h - Math.abs(cvy);
                if (coverlapX >= coverlapY) {
                    if(cvy > 0){
//                        this.y = this.y + coverlapY;
                    } else {
//                        if(Char.x>this.x && ((this.x+this.w)<(Char.x+65)))

                            if ((Char.x < this.x) && ((this.x + this.w) < (Char.x + 101)) && (!this.s) && ((this.y+this.w < 390 )) && !(this.x < this.cx + 129 && this.x + this.w > this.cx && this.y < this.cy + 79 && this.h + this.y > this.cy)) {
                                this.x = this.x1;
                                if(this.sprite != Assets.black_fruit)
                                    Fruit.fl--;
                                else
                                    Fruit.fl+=5;
                                if(this.sprite != Assets.black_fruit) {
                                    Fruit.score += 50;
                                    Fruit.fs = true;
                                    if(GameWorld.aud) {
                                        Assets.trow.play(1);
                                        Assets.char_s.play(1);
                                    }
                                }
                         }
//                        this.y = this.y - coverlapY;
                    }
                } else {
                    if(cvx > 0){
//                        this.x = this.x + coverlapX;
                    } else {
//                        this.x = this.x - coverlapX;
                    }
                }
            }
        }







    }
}
