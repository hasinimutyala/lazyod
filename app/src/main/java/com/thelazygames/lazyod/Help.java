package com.thelazygames.lazyod;

public class Help extends GameView {
    int m=20;
    public Help(Game game){
        super(game);
    }
    @Override
    public void update(float dt){
        Touc t= game.getTouch();
        if(t.up){
            t.up = false;
            game.setGameView(new GameWorld(game));
            return;
        }
        if(m==0){
            m=20;
        }
        else{
            m--;
        }

    }
    @Override
    public void draw(float dt){
        Graphics g = game.getGraphics();
        g.drawBmp(Assets.help, 0, 0);
        if(m <= 10)
            g.drawBmp(Assets.ttc, 271, 370);

    }
    @Override
    public void pause(){

    }
    @Override
    public void resume(){

    }
    @Override
    public void dispose(){

    }
}
