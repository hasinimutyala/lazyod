package com.thelazygames.lazyod;

public class Char {
    static int x,y;
    int ax,ay;
    boolean dir;
    static Bmp sprite;
    int m;
    //odcrow
    int odx,ody,odx1,odx2,odax;
    boolean odstatus;
    double odvex,odvey;
    boolean odd;
    static int ll;
    boolean llc;
    boolean ocs;
    Bmp odsprite;
    Boolean red;
    double tp;
    double time;
    double ftime;
    //odcrow
    double odtp;
    double odtime;
    double odftime;
    boolean coll;
    Touc touch;
   public Char(int x, int y, int x2, Game game){
       Char.sprite = Assets.charc;
       Char.x=400;
       Char.y=260;
       ax=0;
       ay=0;
       dir=false;
       m=50;
       this.ocs = false;
       this.odx = -500;
       this.ody = y;
       this.odx1 = x;
       this.odx2 = x2;
       Char.ll=5;
       red = false;
       this.odstatus = false;
       this.odvex=0;
       this.odvey=0;
       this.odd = false;
       this.llc = false;
       this.odsprite=Assets.crow_fly;
       this.tp = 1f/60;
       this.time = 5;
       this.ftime= 0.05;
//odcrow
       this.odtp = 1f/60;
       this.odtime = 5;
       this.odftime = 0.15;
       this.coll=false;
       this.touch = game.getTouch();
   }
public void draw(Game game) {
    Graphics g = game.getGraphics();
    if(Char.sprite == Assets.charc) {
        if (!this.dir)
            g.drawBmp(sprite, Char.x, Char.y, this.ax, 0, 101, 134);
        else
            g.drawBmp(Assets.charcf, Char.x, Char.y, this.ax, 0, 101, 134);
    }else
        g.drawBmp(Char.sprite, Char.x, Char.y);




//odcrow
    if((this.odsprite == Assets.crow_fly)||(this.odsprite == Assets.crow_fly_flip))
        g.drawBmp(this.odsprite,this.odx,this.ody, this.odax, 0, 129, 79);
    else
        g.drawBmp(this.odsprite,this.odx,this.ody);
    for(int i=0;i<Char.ll;i++){
        g.drawBmp(Assets.health,75+(i*35),18);
    }
    if(this.red)
        g.drawBmp(Assets.red,0,0);


 }


public void update() {
    time += tp;
    if ((Char.x <= 700) && (!dir)) {
        if(this.ocs) {
            Char.sprite = Assets.char_dead;
        }
        else if(Fruit.fs) {

            Char.sprite = Assets.char_smile;
        }
        else {
                Char.sprite = Assets.charc;
                Char.x++;
        }
    }
    else {
        dir = true;
        if (Char.x >= 430) {
            if(this.ocs)
                Char.sprite = Assets.char_dead_flip;
            else if(Fruit.fs) {
                Char.sprite = Assets.char_smile_flip;
            }
            else{
                    Char.sprite = Assets.charc;
                    Char.x--;
            }

        }
        if (Char.x == 430)
            dir = false;
    }
    if(Char.sprite == Assets.charc){
    while (time > ftime) {
        time -= ftime;
        if (!dir) {
            if (this.ax == 0)
                this.ax = 1212;
            else
                this.ax -= 101;
        } else {
            if (this.ax == 1212)
                this.ax = 0;
            else
                this.ax += 101;
        }

    }
}


















//odcrow
    odtime += odtp;
    if(this.odx >= -300 && this.odx <= 400 && !this.odd)
        this.red = true;
    else
        this.red = false;



    if(this.odx >= this.odx1) {
        if ((this.odsprite == Assets.crow_fly) || (this.odsprite == Assets.crow_fly_flip)) {
            if ((this.odx <= this.odx2) && (!this.odd)) {
//        this.x+=10;
                this.odstatus = false;
                if ((Char.x+20) < this.odx + 129 && (Char.x+20) + 65 > this.odx && Char.y < this.ody + 79 && 134 + Char.y > this.ody) {
                    this.ocs = true;
                    coll = true;
                    if(!this.llc){
                        this.llc=true;
                        Char.ll--;
                        Fruit.score -=10;
                        if(GameWorld.aud)
                            Assets.crow_dead.play(1);
//                              Assets.char_c.play(1);
                    }


                } else {
                    if (!this.coll) {
                        this.odvex = (Char.x - this.odx) * 1.3;
                        this.odvey = (Char.y - this.ody) * 1.3;
                    }
                }
                if (coll) {
                    this.odvex = ((this.odx2 + 100) - this.odx) * 1.2;
                    this.odvey = (90 - this.ody) * 1.2;
                }
                this.odx += this.odvex * 0.04;
                this.ody += this.odvey * 0.04;
            } else {
                coll = false;
                this.odd = true;
                if (this.odx >= this.odx1) {
                    this.odsprite = Assets.crow_fly_flip;
                    this.odx -= 15;
                    this.odstatus = true;
                    this.ocs = false;
                }
                if (this.odx <= this.odx1) {
                    this.odd = false;
                    this.odsprite = Assets.crow_fly;
                    this.odx = -500;
                    this.llc = false;
                    this.ocs = false;
                }

            }
            while (odtime > odftime) {
                odtime -= odftime;
                if (!this.odd) {
                    if (this.odax == 0)
                        this.odax = 131;
                    else
                        this.odax = 0;
                } else {
                    if (this.odax == 131)
                        this.odax = 0;
                    else
                        this.odax = 131;
                }
            }
        }
    }
    else{
        this.odx++;
    }


    if(touch.type==0){
        if (touch.x < this.odx + 129 && touch.x > this.odx && touch.y < this.ody + 79 && touch.y > this.ody) {
            if(!this.odd) {
                this.odsprite = Assets.dead_crow;
            }
            else {
                this.odsprite = Assets.dead_crow_flip;
            }
            if(GameWorld.aud) {
                Assets.touch.play(1);
                Assets.char_c.play(1);
            }
        }
    }

    if((this.odsprite == Assets.dead_crow) || (this.odsprite == Assets.dead_crow_flip))
    {
        this.ody+=10;
    }

    if(this.ody > 480)
    {
        this.ocs = false;
        this.coll = false;
        this.odsprite = Assets.crow_fly;
        this.odx = -500;
        this.ody = 80;
        this.odd = false;
        if(this.odstatus)
            Fruit.score -=10;
        else
            Fruit.score += 50;
    }




}
}