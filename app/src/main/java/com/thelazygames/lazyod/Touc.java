package com.thelazygames.lazyod;

import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.View;

public class Touc implements OnTouchListener {
    boolean down,drag,up;
    int type;
    int x,y;
    float sx;
    float sy;
    float mT;
    float mL;
    public Touc(View view, float sx, float sy, float mT, float mL){
        view.setOnTouchListener(this);
        this.sx = sx;
        this.sy = sy;
        this.mT = mT;
        this.mL = mL;
        type = -1;
    }
    public boolean onTouch(View v, MotionEvent event) {
        synchronized (this) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    this.type = 0;
                    this.down = true;
                    break;
                case MotionEvent.ACTION_MOVE:
                    this.type = 1;
                    this.drag = true;
                    break;
                case MotionEvent.ACTION_UP:
                    this.type = 2;
                    this.down = false;
                    this.drag = false;
                    this.up = true;
                    break;
                case MotionEvent.ACTION_CANCEL:
                    this.type = -1;
                    break;
            }
           this.x = (int)((event.getX()-this.mL) * this.sx);
           this.y = (int)((event.getY()-this.mT) * this.sy);
            return true;
        }
    }


}
