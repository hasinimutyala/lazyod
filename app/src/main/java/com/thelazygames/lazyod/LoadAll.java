package com.thelazygames.lazyod;

public class LoadAll extends GameView {
    public LoadAll(Game game){
        super(game);
    }
    public void update(float dt){
        Graphics g = game.getGraphics();
        Assets.title = g.newBmp("title.png", Bmp.BmpFormat.RGB_565);
        Assets.help = g.newBmp("help.png", Bmp.BmpFormat.RGB_565);
        Assets.tts = g.newBmp("tts.png", Bmp.BmpFormat.ARGB_4444);
        Assets.ttc = g.newBmp("ttc.png", Bmp.BmpFormat.ARGB_4444);
        Assets.bg = g.newBmp("bg.png", Bmp.BmpFormat.RGB_565);
        Assets.bg2 = g.newBmp("bg2.png", Bmp.BmpFormat.ARGB_4444);

        Assets.c1 = g.newBmp("c1.png", Bmp.BmpFormat.ARGB_4444);
        Assets.c2 = g.newBmp("c2.png", Bmp.BmpFormat.ARGB_4444);
        Assets.c3 = g.newBmp("c3.png", Bmp.BmpFormat.ARGB_4444);
        Assets.c4 = g.newBmp("c4.png", Bmp.BmpFormat.ARGB_4444);
        Assets.c5 = g.newBmp("c5.png", Bmp.BmpFormat.ARGB_4444);

        Assets.l_tile = g.newBmp("l_tile.png", Bmp.BmpFormat.ARGB_4444);
        Assets.r_tile = g.newBmp("r_tile.png", Bmp.BmpFormat.ARGB_4444);

        Assets.black_fruit = g.newBmp("black_fruit.png", Bmp.BmpFormat.ARGB_4444);
        Assets.blue_fruit = g.newBmp("blue_fruit.png", Bmp.BmpFormat.ARGB_4444);
        Assets.red_fruit = g.newBmp("red_fruit.png", Bmp.BmpFormat.ARGB_4444);
        Assets.orange_fruit = g.newBmp("orange_fruit.png", Bmp.BmpFormat.ARGB_4444);
        Assets.green_fruit = g.newBmp("green_fruit.png", Bmp.BmpFormat.ARGB_4444);


        Assets.charc = g.newBmp("char_run.png", Bmp.BmpFormat.ARGB_4444);
        Assets.charcf = g.newBmp("char_run_flip.png", Bmp.BmpFormat.ARGB_4444);
        Assets.crow_fly = g.newBmp("crow_fly.png", Bmp.BmpFormat.ARGB_4444);
        Assets.crow_fly_flip = g.newBmp("crow_fly_flip.png", Bmp.BmpFormat.ARGB_4444);
        Assets.dead_crow = g.newBmp("dead_crow.png", Bmp.BmpFormat.ARGB_4444);
        Assets.dead_crow_flip = g.newBmp("dead_crow_flip.png", Bmp.BmpFormat.ARGB_4444);
        Assets.board = g.newBmp("f_l.png", Bmp.BmpFormat.ARGB_4444);
        Assets.health = g.newBmp("health.png", Bmp.BmpFormat.ARGB_4444);
        Assets.buttons = g.newBmp("buttons.png", Bmp.BmpFormat.ARGB_4444);
        Assets.gameover = g.newBmp("gameover.png", Bmp.BmpFormat.ARGB_4444);
        Assets.l_c = g.newBmp("level_completed.png", Bmp.BmpFormat.ARGB_4444);
        Assets.rock = g.newBmp("rock.png", Bmp.BmpFormat.ARGB_4444);
        Assets.grass = g.newBmp("grass.png", Bmp.BmpFormat.ARGB_4444);
        Assets.resume = g.newBmp("resume.png", Bmp.BmpFormat.ARGB_4444);
        Assets.char_dead = g.newBmp("char_dead.png", Bmp.BmpFormat.ARGB_4444);
        Assets.char_smile = g.newBmp("char_smile.png", Bmp.BmpFormat.ARGB_4444);
        Assets.char_dead_flip = g.newBmp("char_dead_flip.png", Bmp.BmpFormat.ARGB_4444);
        Assets.char_smile_flip = g.newBmp("char_smile_flip.png", Bmp.BmpFormat.ARGB_4444);
        Assets.red = g.newBmp("red.png", Bmp.BmpFormat.ARGB_4444);
        Assets.mute= g.newBmp("mute.png", Bmp.BmpFormat.ARGB_4444);
        Assets.touch = game.getAudio().newSound("touch.ogg");
        Assets.trow = game.getAudio().newSound("throw.ogg");
        Assets.crow_dead = game.getAudio().newSound("crow_dead.ogg");
        Assets.crow_caw = game.getAudio().newSound("crow_caw.ogg");
        Assets.crow_caw_e = game.getAudio().newSound("crow_caw_e.ogg");
        Assets.char_s = game.getAudio().newSound("char_smile.ogg");
        Assets.char_c = game.getAudio().newSound("char_cry.ogg");

        game.setGameView(new MainScreen(game));
    }
    public void draw(float dt){

    }
    public void pause(){

    }
    public void resume(){

    }
    public void dispose(){

    }

}
