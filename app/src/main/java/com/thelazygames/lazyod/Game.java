package com.thelazygames.lazyod;

public interface Game {
    public Touc getTouch();
    public Graphics getGraphics();
    public Audio getAudio();
    public void setGameView(GameView gameView);
    public GameView getCurrentGameView();
    public void finish();
    public void setScore(int s);
    public int getScore();
}
