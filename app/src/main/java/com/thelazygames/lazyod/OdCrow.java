package com.thelazygames.lazyod;

public class OdCrow {
   int odx,ody,odx1,odx2,odax;
    boolean odstatus;
    double odvex,odvey;
   boolean odd;
    static int ll;
    boolean llc;
    static boolean ocs;
   Bmp odsprite;
    Touc touch;
    Boolean red;
  public OdCrow(int x, int y, int odx2, Game game){
      this.touch = game.getTouch();
      this.odx = -500;
      this.ody = y;
      this.odx1 = x;
      this.odx2 = odx2;
      OdCrow.ll=5;
      OdCrow.ocs = false;
      red = false;
      this.odstatus = false;
      this.odvex=0;
      this.odvey=0;
      this.odd = false;
      this.llc = false;
      this.odsprite=Assets.crow_fly;
  }
  public void draw(Game game){

      Graphics g = game.getGraphics();
      if((this.odsprite == Assets.crow_fly)||(this.odsprite == Assets.crow_fly_flip))
      g.drawBmp(this.odsprite,this.odx,this.ody, this.odax, 0, 129, 79);
      else
          g.drawBmp(this.odsprite,this.odx,this.ody);
        for(int i=0;i<OdCrow.ll;i++){
          g.drawBmp(Assets.health,75+(i*35),18);
      }
    if(this.red)
        g.drawBmp(Assets.red,0,0);

  }
    double odtp = 1f/60;
    double odtime = 5;
    double odftime = 0.15;
 boolean coll=false;

  public void update() {
      odtime += odtp;
      if(this.odx >= -300 && this.odx <= 400 && !this.odd)
          this.red = true;
      else
      this.red = false;



      if(this.odx >= this.odx1) {
          if ((this.odsprite == Assets.crow_fly) || (this.odsprite == Assets.crow_fly_flip)) {
              if ((this.odx <= this.odx2) && (!this.odd)) {
//        this.x+=10;
                this.odstatus = false;
                  if ((Char.x+20) < this.odx + 129 && (Char.x+20) + 65 > this.odx && Char.y < this.ody + 79 && 134 + Char.y > this.ody) {
                      OdCrow.ocs = true;
                      coll = true;
                      if(!this.llc){
                          this.llc=true;
                          OdCrow.ll--;
                          Fruit.score -=10;
                          if(GameWorld.aud)
                              Assets.crow_dead.play(1);
//                              Assets.char_c.play(1);
                      }


                  } else {
                      if (!this.coll) {
                          this.odvex = (Char.x - this.odx) * 1.3;
                          this.odvey = (Char.y - this.ody) * 1.3;
                      }
                  }
                  if (coll) {
                      this.odvex = ((this.odx2 + 100) - this.odx) * 1.2;
                      this.odvey = (90 - this.ody) * 1.2;
                  }
                  this.odx += this.odvex * 0.04;
                  this.ody += this.odvey * 0.04;
              } else {
                  coll = false;
                  this.odd = true;
                  if (this.odx >= this.odx1) {
                      this.odsprite = Assets.crow_fly_flip;
                      this.odx -= 15;
                      this.odstatus = true;
                      OdCrow.ocs = false;
                  }
                  if (this.odx <= this.odx1) {
                      this.odd = false;
                      this.odsprite = Assets.crow_fly;
                      this.odx = -500;
                      this.llc = false;
                      OdCrow.ocs = false;
                  }

          }
              while (odtime > odftime) {
                  odtime -= odftime;
                  if (!this.odd) {
                      if (this.odax == 0)
                          this.odax = 131;
                      else
                          this.odax = 0;
                  } else {
                      if (this.odax == 131)
                          this.odax = 0;
                      else
                          this.odax = 131;
                  }
              }
          }
      }
        else{
          this.odx++;
      }


      if(touch.type==0){
          if (touch.x < this.odx + 129 && touch.x > this.odx && touch.y < this.ody + 79 && touch.y > this.ody) {
              if(!this.odd) {
                  this.odsprite = Assets.dead_crow;
              }
              else {
                  this.odsprite = Assets.dead_crow_flip;
              }
              if(GameWorld.aud) {
                  Assets.touch.play(1);
                  Assets.char_c.play(1);
              }
          }
      }

      if((this.odsprite == Assets.dead_crow) || (this.odsprite == Assets.dead_crow_flip))
      {
          this.ody+=10;
      }

      if(this.ody > 480)
      {
          OdCrow.ocs = false;
          this.coll = false;
          this.odsprite = Assets.crow_fly;
          this.odx = -500;
          this.ody = 80;
          this.odd = false;
          if(this.odstatus)
              Fruit.score -=10;
          else
          Fruit.score += 50;
      }
  }
}
