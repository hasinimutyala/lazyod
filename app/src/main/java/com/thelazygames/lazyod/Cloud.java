package com.thelazygames.lazyod;

public class Cloud {
    int s,x,y;
    Bmp sprite;
    public Cloud(int s, int x, int y, Bmp sprite){
        this.s=s;
        this.x=x;
        this.y=y;
        this.sprite = sprite;
    }

    public void update(){
        this.x +=this.s;
        if(this.x > 800)
        {
            this.y = (int)Math.floor((Math.random()*200)+1);
            int t = (int)Math.floor((Math.random()*5)+1);
            if(t==1)
                this.sprite = Assets.c1;
            if(t==2)
                this.sprite = Assets.c2;
            if(t==3)
                this.sprite = Assets.c3;
            if(t==4)
                this.sprite = Assets.c4;
            if(t==5)
                this.sprite = Assets.c5;
            this.x = -this.sprite.getWidth()+10;
        }
    }

    public void draw(Game game){
        Graphics g = game.getGraphics();
        g.drawBmp(this.sprite,this.x,this.y);
    }

}
