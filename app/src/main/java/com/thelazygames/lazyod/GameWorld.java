package com.thelazygames.lazyod;

public class GameWorld extends GameView {
    boolean ps;
    boolean gover;
    boolean lc;
    static boolean aud;
    Cloud cloud;
    Cloud cloud2;
    Fruit f1;
    Fruit f2;
    Fruit f3;
    Fruit f4;
    Char charc;
    Crow c1;
    Rock rock;
    Touc touch;
    public GameWorld(Game game){
        super(game);
        ps = false;
        gover = false;
        lc = false;
        cloud = new Cloud(1,-100,173,Assets.c3);
        cloud2 = new Cloud(2,-100,273,Assets.c5);
        f1 = new Fruit(-700,10,Assets.green_fruit,game,-1000,10,900);
        f2 = new Fruit(-300,10,Assets.red_fruit,game,-1000,10,900);
        f4 = new Fruit(-100,10,Assets.orange_fruit,game,-1000,10,900);
        f3 = new Fruit(-900,10,Assets.black_fruit,game,-1000,10,900);
        c1 = new Crow(-200, 80, 900, game);
        charc= new Char(-200, 80, 900, game);
        rock = new Rock();
        aud = true;
        touch = game.getTouch();
    }

    @Override
    public void update(float dt) {


        if (ps) {
            if (touch.type == 2 && touch.x > 260 && touch.x < 396 && touch.y > 172 && touch.y < 307)
                ps = false;
            if (touch.type == 2 && touch.x > 407 && touch.x < 543 && touch.y > 172 && touch.y < 307)
                game.finish();
        }
        if(lc){
            game.setScore(Fruit.score);
            if (touch.type == 2 && touch.x > 271 && touch.x < 516 && touch.y > 270 && touch.y < 322)
                game.setGameView(new GameWorld(game));
        }
        if(gover){
//            OdCrow.ocs = true;
            game.setScore(Fruit.score);
            if (touch.type == 2 && touch.x > 271 && touch.x < 516 && touch.y > 270 && touch.y < 322) {
                gover = false;
                game.setGameView(new GameWorld(game));
            }
            if (touch.type == 2 && touch.x > 271 && touch.x < 516 && touch.y > 330 && touch.y < 382){
                game.setGameView(new Help(game));
            }
        }
        if (!ps && !lc && !gover) {
            if(Char.ll<0)
                gover=true;
//            if(cloud.x>400)
            if(this.touch.type == 2) {
                if (this.touch.x > 669 && this.touch.x < 731 && this.touch.y > 4 && this.touch.y < 67) {
                    if (aud)
                        aud = false;
                    else
                        aud = true;
                }
            }

            if(touch.type==2 && touch.x>734 && touch.x<796 && touch.y>4 && touch.y<67)
                ps = true;
            if(Fruit.fl <= 0)
                lc=true;
            cloud.update();
            cloud2.update();
            charc.update();
            f1.update(game);
            f2.update(game);
            f3.update(game);
            f4.update(game);
            c1.update();
            rock.update();
        }
    this.touch.type = -1;
        this.touch.x = 0;
        this.touch.y = 0;
    }
    @Override
    public void draw(float dt){
        Graphics g = game.getGraphics();
        g.drawBmp(Assets.bg, 0, 0);
        cloud.draw(game);
        cloud2.draw(game);
        g.drawBmp(Assets.bg2, 0, 0);
        g.drawBmp(Assets.l_tile, -250, 380);
        g.drawBmp(Assets.r_tile, 400, 380);
        g.drawBmp(Assets.grass,140,285);
        rock.draw(game);
        f1.draw(game);
        f2.draw(game);
        f3.draw(game);
        f4.draw(game);
        charc.draw(game);
        c1.draw(game);

        if(!ps && !lc && !gover) {
            g.drawBmp(Assets.buttons, 669, 4);
            if(!GameWorld.aud)
            g.drawBmp(Assets.mute,669, 4);
        }

        if(ps)
            g.drawBmp(Assets.resume,260,172);
        g.drawBmp(Assets.board, 4, 4);
        if(Fruit.fl>=10)
            g.drawBmp(Integer.toString(Fruit.fl),14,55,40);
        else
            g.drawBmp(Integer.toString(Fruit.fl),24,55,40);
        if(lc) {
            g.drawBmp(Assets.l_c, 92, 73);
            g.drawBmp(Integer.toString(Fruit.score),330,199,40);
            g.drawBmp(Integer.toString(game.getScore()),330,248,40);
        }
        if(gover){
            g.drawBmp(Assets.gameover,92,73);
            g.drawBmp(Integer.toString(Fruit.score),330,201,40);
            g.drawBmp(Integer.toString(game.getScore()),330,250,40);
        }
    }
    @Override
    public void pause(){

    }
    @Override
    public void resume(){

    }
    @Override
    public void dispose(){

    }
}
